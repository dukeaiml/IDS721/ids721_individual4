use lambda_runtime::{run, Error, Context};
use serde::{Deserialize, Serialize};
use rusoto_core::{Region, RusotoError};
use rusoto_s3::{GetObjectRequest, S3Client, S3};
use csv::ReaderBuilder;


/// 请求结构体，用于反序列化 Lambda 事件的有效负载。
#[derive(Debug, Deserialize)]
struct Request {
    bucket_name: String,
    object_key: String,
}

/// 响应结构体，用于序列化成 Lambda 函数的响应。
#[derive(Debug, Serialize)]
struct Response {
    ages: Vec<u32>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct UserData {
    Education: String,
    Experience: u32,
    Location: String,
    Job_Title: String,
    Age: u32,
    Gender: String,
    Salary: f64,
}

async fn fetch_data_from_s3(bucket_name: &str, object_key: &str) -> Result<Vec<u32>, RusotoError<rusoto_s3::GetObjectError>> {
    let region = Region::default();
    let s3_client = S3Client::new(region);

    let request = GetObjectRequest {
        bucket: bucket_name.to_string(),
        key: object_key.to_string(),
        ..Default::default()
    };

    let response = s3_client.get_object(request).await?;
    let body = response.body.unwrap().into_async_read();

    let mut rdr = ReaderBuilder::new()
        .has_headers(true)
        .from_reader(body);

    let mut ages: Vec<u32> = Vec::new();

    // 遍历 CSV 文件中的每一行数据，并将年龄加入到列表中
    for result in rdr.deserialize::<UserData>() {
        let record: UserData = result?;
        ages.push(record.Age);
    }

    Ok(ages)
}

async fn function_handler(event: Request, _ctx: Context) -> Result<Response, Error> {
    // 从请求中获取 S3 存储桶名称和对象键名
    let bucket_name = event.bucket_name;
    let object_key = event.object_key;

    // 从 S3 获取数据
    match fetch_data_from_s3(&bucket_name, &object_key).await {
        Ok(ages) => {
            // 返回年龄列表作为响应
            Ok(Response { ages })
        },
        Err(err) => {
            // 处理错误
            eprintln!("Error fetching data from S3: {:?}", err);
            Err("Error fetching data from S3".into())
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // 运行 Lambda 函数
    run(function_handler).await?;
    Ok(())
}
