use aws_lambda_events::event::s3::S3Event;
use lambda_runtime::{run, Error, Context};
use std::error::Error;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct UserData {
    Education: String,
    Experience: u32,
    Location: String,
    Job_Title: String,
    Age: u32,
    Gender: String,
    Salary: f64,
}

async fn function_handler(user_data_list: Vec<UserData>, _ctx: Context) -> Result<Vec<u32>, Error> {
    // 提取所有年龄并返回
    let ages: Vec<u32> = user_data_list.iter().map(|user_data| user_data.Age).collect();
    Ok(ages)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // 启动 Lambda 运行时
    run(function_handler).await?;
    Ok(())
}
