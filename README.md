# IDS721_individual4



## Overview

Rust AWS Lambda function
Step Functions workflow coordinating Lambdas
Orchestrate data processing pipeline

This project deployed 2 lambda functions on aws step functions framework. It can fetch the data from a s3 bucket and then extract data from a csv file and then transform the extracted data.

## Rust Lambda Function

1. fetch data from s3 bucket

![Alt text](image.png)

```rust
use lambda_runtime::{run, Error, Context};
use serde::{Deserialize, Serialize};
use rusoto_core::{Region, RusotoError};
use rusoto_s3::{GetObjectRequest, S3Client, S3};
use csv::ReaderBuilder;


#[derive(Debug, Deserialize)]
struct Request {
    bucket_name: String,
    object_key: String,
}

#[derive(Debug, Serialize)]
struct Response {
    ages: Vec<u32>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct UserData {
    Education: String,
    Experience: u32,
    Location: String,
    Job_Title: String,
    Age: u32,
    Gender: String,
    Salary: f64,
}

async fn fetch_data_from_s3(bucket_name: &str, object_key: &str) -> Result<Vec<u32>, RusotoError<rusoto_s3::GetObjectError>> {
    let region = Region::default();
    let s3_client = S3Client::new(region);

    let request = GetObjectRequest {
        bucket: bucket_name.to_string(),
        key: object_key.to_string(),
        ..Default::default()
    };

    let response = s3_client.get_object(request).await?;
    let body = response.body.unwrap().into_async_read();

    let mut rdr = ReaderBuilder::new()
        .has_headers(true)
        .from_reader(body);

    let mut ages: Vec<u32> = Vec::new();


    for result in rdr.deserialize::<UserData>() {
        let record: UserData = result?;
        ages.push(record.Age);
    }

    Ok(ages)
}

async fn function_handler(event: Request, _ctx: Context) -> Result<Response, Error> {

    let bucket_name = event.bucket_name;
    let object_key = event.object_key;


    match fetch_data_from_s3(&bucket_name, &object_key).await {
        Ok(ages) => {

            Ok(Response { ages })
        },
        Err(err) => {

            eprintln!("Error fetching data from S3: {:?}", err);
            Err("Error fetching data from S3".into())
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {

    run(function_handler).await?;
    Ok(())
}

```

2. transform data 

Get the vector from the last step lambda function 

```rust
use aws_lambda_events::event::s3::S3Event;
use lambda_runtime::{run, Error, Context};
use std::error::Error;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
struct UserData {
    Education: String,
    Experience: u32,
    Location: String,
    Job_Title: String,
    Age: u32,
    Gender: String,
    Salary: f64,
}

async fn function_handler(user_data_list: Vec<UserData>, _ctx: Context) -> Result<Vec<u32>, Error> {
    // 提取所有年龄并返回
    let ages: Vec<u32> = user_data_list.iter().map(|user_data| user_data.Age).collect();
    Ok(ages)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // 启动 Lambda 运行时
    run(function_handler).await?;
    Ok(())
}


```

3. Deploy lambda 

To test the function

```bash 
cargo lambda watch
cargo lambda invoke <function name> --data-example apigw-request
```


To build&deploy

```bash
 cargo lambda build --release (<option>--arm64 )
```

```bash
cargo lambda deploy 
```

## AWS Step Function

State Machine :


```json
{
  "Comment": "A simple state machine for data processing",
  "StartAt": "FetchData",
  "States": {
    "FetchData": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:558669332806:function:fetch_data",
      "Next": "TransformData"
    },
    "TransformData": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-2:558669332806:function:transform_data",
      "End": true
    }
  }
}


```


## Test

Test command:
```json
{
    "bucket_name":"salary-prediction-data",
    "object_key":"salary_prediction_data.csv"
}
```


Result:

```json

{"age": "34"}

```